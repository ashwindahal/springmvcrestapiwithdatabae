package com.cerotid.bank.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cerotid.bank.dao.BankDAO;
import com.cerotid.bank.dao.DatabaseClass;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;

@Component
public class BankService {

	@Autowired
	BankDAO bankDao;

	private Map<Long, Customer> customers = DatabaseClass.getMessages();

	public BankService() {
		customers.put(1L, new Customer(1, "Ashwin", "Nepal", "123456789",
				new Address("3725 Towne Crossing Blvd", "Mesquite", "75150", "Tx")));
		customers.put(2L, new Customer(2, "Steven", "Thomas", "987654321",
				new Address("1224 Qual Dr", "Balch Springs", "75180", "Tx")));

	}

	// gets all customers
	public List<Customer> getAllCustomers() {
		//return new ArrayList<Customer>(customers.values());
		return bankDao.getCustomers(); 
	}

	// gets customer by id
	public Customer getCustomer(long id) {
		return customers.get(id);
	}

	// add's customer to database
	public Customer addCustomer(Customer customer) {
		customer.setId(customers.size() + 1);
		customers.put(customer.getId(), customer);
		return customer;

	}

	// updates customer by id
	public Customer updateMessage(Customer customer) {
		if (customer.getId() <= 0) {
			return null;
		}
		customers.put(customer.getId(), customer);
		return customer;

	}

	public Customer removeMessage(long id) {
		return customers.remove(id);
	}

}
