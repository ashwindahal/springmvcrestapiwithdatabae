package com.cerotid.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cerotid.bank.model.Customer;
import com.cerotid.bank.service.BankService;

@RestController
public class BankRestController {

	// injecting dependency on BankService Objects
	@Autowired
	BankService bankService;

	// gets all customers
	@GetMapping("/customers")
	public List<Customer> getCustomers() {
		return bankService.getAllCustomers();
	}

	// gets a customer by id
	@GetMapping("/customers/{customerId}")
	public Customer getCustomerById(@PathVariable("customerId") long id) {
		return bankService.getCustomer(id);

	}

	@PostMapping(path = "/customers")
	public Customer addCustomer(Customer customer) {
		return bankService.addCustomer(customer);

	}

	@DeleteMapping("/customers/{customerId}")
	// @Produces(MediaType.APPLICATION_JSON)
	public void deleteCustomer(@PathVariable("customerId") long Id) {
		bankService.removeMessage(Id);
	}

}
