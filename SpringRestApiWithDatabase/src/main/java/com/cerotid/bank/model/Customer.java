package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Customer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2713843867520012404L;
	private long id;
	private String firstName;
	private String lastName;
	private String ssn;
	private Address address;
	private ArrayList<Account> accounts;

	public Customer() {

	}

	public Customer(String firstName, String lastName, String ssn, Address address) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;
	}

	public Customer(long id, String firstName, String lastName, String ssn, Address address) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
		this.address = address;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
